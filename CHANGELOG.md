# Change Log
All enhancements and patches to manax-app-base will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [0.4] 2019-06-22

### Added
- hooks folder

### Changed
- urls splitted into web and rest

### Fixed
- app_name instead of project_name in README.md
- fix celery integration (set default_app_config)

## [0.3] 2018-12-27

### Added
- serializers.py
- tasks.py

### Fixed
- several typos

## [0.2] 2018-09-13

### Added
- forms.py
- static files
    - javascript 
    - css

## [0.1] 2018-08-23

### Added
- app schema
