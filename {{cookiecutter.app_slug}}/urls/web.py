from django.urls import path

from {{ cookiecutter.app_slug }}.views import HomeView

urlpatterns = [
    # URL pattern for the HomeView
    path(
        route = '',
        view = HomeView,
        name = 'home'
    ),
]