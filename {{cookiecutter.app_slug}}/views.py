from django.shortcuts import render

def HomeView(request):
    """
    """

    context = {}
    return render(request, '{{ cookiecutter.app_slug }}/home.html', context) 
