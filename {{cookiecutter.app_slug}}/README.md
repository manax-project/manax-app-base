# manax install info

## edit ../../config/settings/base.py

LOCAL_APPS = (  
    [...]  
    # {{cookiecutter.app_slug}}  
    '{{cookiecutter.app_slug}}.apps.{{cookiecutter.app_name}}Config',  
    [...]  
)  

## edit ../../config/urls.py

urlpatterns = [  
    [...]  
    # {{cookiecutter.app_slug}}  
    ## web
    path('{{cookiecutter.app_slug}}/', include(('{{cookiecutter.app_slug}}.urls.web', '{{cookiecutter.app_slug}}'), namespace='{{cookiecutter.app_slug}}-web')),  
    ## rest
    path('api/{{cookiecutter.app_slug}}/', include(('{{cookiecutter.app_slug}}.urls.rest', '{{cookiecutter.app_slug}}'), namespace='{{cookiecutter.app_slug}}-rest')),   
    [...]  
]