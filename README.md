# manax-app-base

basic template for manax app

## FEATURES

## USAGE

```bash
cd apps
# devel branch
cookiecutter --overwrite-if-exists https://gitlab.com/manax-project/manax-app-base.git --checkout develop
# master
cookiecutter --overwrite-if-exists https://gitlab.com/manax-project/manax-app-base.git
# latest release
cookiecutter --overwrite-if-exists https://gitlab.com/manax-project/manax-app-base.git --checkout 0.4
```
